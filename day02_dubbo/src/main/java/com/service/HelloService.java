package com.service;

import com.domain.User;

import java.util.List;

public interface HelloService {

    public void sayHello();
    public List<User> findAll();
}
