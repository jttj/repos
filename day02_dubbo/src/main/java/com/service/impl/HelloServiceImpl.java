package com.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.dao.UserDao;
import com.domain.User;
import com.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service(interfaceClass = HelloService.class)
@Transactional
public class HelloServiceImpl implements HelloService {

    @Autowired
    private UserDao userDao;
    @Override
    public void sayHello() {
        System.out.println("hello");
    }

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }


}
